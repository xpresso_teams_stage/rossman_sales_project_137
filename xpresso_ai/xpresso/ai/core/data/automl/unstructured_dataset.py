""" Class design for Dataset"""
import copy
import json
import os
import shutil

from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    SerializationFailedException, DeserializationFailedException
from xpresso.ai.core.commons.utils.constants import DOWNLOAD_DIR, json_extension
from xpresso.ai.core.commons.utils.constants import NGRAMS
from xpresso.ai.core.commons.utils.xpr_config_parser import XprConfigParser
from xpresso.ai.core.data.automl.dataset import AbstractDataset, \
    DatasetEncoder, DatasetDecoder
from xpresso.ai.core.data.automl.dataset_type import DatasetType
from xpresso.ai.core.data.connections import Connector
from xpresso.ai.core.data.exploration.unstructured_dataset_info import \
    UnstructuredDatasetInfo
from xpresso.ai.core.logging.xpr_log import XprLogger

__all__ = ['UnstructuredDataset']
__author__ = 'Srijan Sharma'

LOGGER = XprLogger()


class UnstructuredDataset(AbstractDataset):
    """ UnstructuredDataset stores the data in a plain file format
    """

    def __init__(self, dataset_name: str = "default",
                 project_name: str = "default_project",
                 created_by: str = "default",
                 description: str = "This is a unstructured automl"):
        super().__init__(dataset_name=dataset_name,
                         description=description,
                         project_name=project_name,
                         created_by=created_by)

        self.info = UnstructuredDatasetInfo()
        self.type = DatasetType.UTEXT
        self.data_path = None

    def import_dataset(self, user_config, local_storage_required: bool = False,
                       sample_percentage: float = 100):
        """ Fetches automl from multiple data sources and loads them
        into a automl"""
        self.data = Connector().getconnector(
            user_datasource=user_config.get("type"),
            datasource_type=user_config.get("data_source")).import_files(
            user_config)
        self.data_path = user_config.get("path")
        if user_config.get("data_source").lower() != "local":
            self.data_path = DOWNLOAD_DIR

    def save(self):
        """ Save the data into the local file system in
        a serialized format

        Returns:
            str: json file path where serialized metadata, metrics has been
            stored
        """
        json_file_path = self.json_serialize_unstructured()
        if os.path.exists(json_file_path):
            shutil.move(json_file_path, self.data_path)
        return self.data_path

    def load(self, directory_path):
        """
        Load the data set from local storage and deserialize to update
        the dataset
        Args:
            directory_path(str): path where json file (i.e. metrics,
            metadata) and data
        """
        json_data_path = ""
        files = os.listdir(directory_path)
        for file in files:
            extension = os.path.splitext(file)[1]
            if extension == json_extension:
                json_data_path = os.path.join(directory_path, file)
                break
        self.json_deserialize_unstructured(json_data_path)

    def json_serialize_unstructured(self):
        """
        Helper function to serialize unstructured dataset object to
        json object
        """
        temp_dataset = copy.deepcopy(self)
        del temp_dataset.data
        if temp_dataset.info.attributeInfo:
            del temp_dataset.info.attributeInfo.logger
        if temp_dataset.info.metrics:
            temp_dataset.info.metrics = self.metric_serialize_deserialize(
                temp_dataset.info.metrics, serialize=True, keys=NGRAMS)
        try:
            json_file_name = self.get_json_file_path()
            with open(json_file_name, 'w', encoding='utf-8') as file:
                json.dump(temp_dataset, file, ensure_ascii=False, indent=4,
                          cls=DatasetEncoder)
        except (TypeError, OverflowError, ValueError):
            raise SerializationFailedException("JSON serialization failed")
        return json_file_name

    def json_deserialize_unstructured(self, json_data_path):
        """
        Helper function to deserialize json object to
        unstructured dataset object
        Args:
            json_data_path(str): json file path to load"""
        try:
            with open(json_data_path) as json_file:
                json_data = json.load(json_file, cls=DatasetDecoder)
        except json.JSONDecodeError:
            raise DeserializationFailedException("JSON deserialization failed")

        for key, value in json_data.items():
            setattr(self, key, value)

        try:
            self.config = XprConfigParser()
            self.config.config_json = json_data["config"]["config_json"]
        except KeyError:
            LOGGER.warning("'Config' key error in json data ")

        try:
            info = json_data["info"]
            self.info = UnstructuredDatasetInfo()
            for key, value in info.items():
                setattr(self.info, key, value)
        except KeyError:
            LOGGER.warning("Info key error in json data")
        self.info.metrics = self.metric_serialize_deserialize(self.info.metrics,
                                                              serialize=False,
                                                              keys=NGRAMS)

    def diff(self):
        pass
