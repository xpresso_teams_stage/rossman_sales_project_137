__all__ = ['RenderExploration']
__author__ = 'Srijan Sharma'

from xpresso.ai.core.data.exploration.data_type import DataType
from xpresso.ai.core.data.automl.dataset_type import DatasetType
from xpresso.ai.core.data.automl import utility
from xpresso.ai.core.logging.xpr_log import XprLogger
from IPython.display import display
import pandas as pd
import numpy as np

# This is indented as logger can not be serialized and can not be part
# of automl
logger = XprLogger()


class RenderExploration:
    def __init__(self, dataset):
        self.dataset = dataset
        self.field_render_numeric = {"na_count": "NA Count",
                                     "na_count_percentage": "NA %", "min":
                                         "min", "max": "max", "mean": "Mean",
                                     "median": "Median",
                                     "std": "Standard Deviation",
                                     "var": "Variance", "mode": "Mode",
                                     "quartiles": "Quartiles",
                                     "iqr": "iqr", "kurtosis": "kurtosis",
                                     "skewness": "skewness",
                                     "is_valid": "Is Valid?"}

        self.field_render_categorical = {"na_count": "NA Count",
                                         "na_count_percentage": "NA %",
                                         "mode": "Mode"}

        self.field_render_date = {"na_count": "NA Count",
                                  "na_count_percentage": "NA %",
                                  "min": "Min", "max": "Max",
                                  "missing_dates": "Missing Dates"}
        self.field_render_multivariate = ["pearson", "spearman", "chi_square"]

    def render_understand(self, verbose=True):
        """
        Outputs explore understand data
        Args:
            verbose('bool'): If True then renders the output
        """
        if not verbose:
            return

        if self.dataset.type is DatasetType.STRUCTURED:
            data = dict()
            for attr in self.dataset.info.attributeInfo:
                data[attr.name] = attr.type
            data_df = pd.DataFrame.from_dict(data, orient="index",
                                             columns=["Datatype"])
            display(data_df)

        elif self.dataset.type is DatasetType.UTEXT:
            data = self.dataset.info.attributeInfo.metrics
            data_df = pd.DataFrame.from_dict(data, orient="index",
                                             columns=["Values"])
            print("\nFile size Analysis:")
            display(data_df)
        return

    def render_univariate(self, verbose=True, to_excel=False,
                          output_path=None,
                          file_name=None):
        """
        Outputs explore univariate data
        Args:
            verbose('bool'): If True then renders the output
            to_excel('bool'): If True saves the output to excel file
            output_path(str): path where the univariate excel file to be stored
            file_name(str): file name of excel file
        """
        if not verbose and not to_excel:
            return
        numeric_dict = dict()
        categorical_dict = dict()
        date_dict = dict()
        string_data = pd.DataFrame()
        text_data = pd.DataFrame()

        if self.dataset.type != DatasetType.STRUCTURED:
            logger.info("Unsupported datatype for render_univariate")
            return

        for attr in self.dataset.info.attributeInfo:
            if attr.type == DataType.NUMERIC.value:
                numeric_dict[attr.name] = dict()
                for field in self.field_render_numeric.keys():
                    if field in attr.metrics.keys():
                        numeric_dict[attr.name][
                            self.field_render_numeric[field]] = attr.metrics[
                            field]
                    else:
                        numeric_dict[attr.name][
                            self.field_render_numeric[field]] = np.nan

                    if hasattr(attr, "type"):
                        numeric_dict[attr.name]["type"] = attr.type

            elif attr.type == DataType.NOMINAL.value or attr.type == \
                    DataType.ORDINAL.value:

                categorical_dict[attr.name] = dict()
                for field in self.field_render_categorical:
                    if field in attr.metrics.keys():
                        categorical_dict[attr.name][
                            self.field_render_categorical[field]] = \
                            attr.metrics[field]
                    else:
                        categorical_dict[attr.name][
                            self.field_render_categorical[field]] = np.nan

                    if hasattr(attr, "type"):
                        categorical_dict[attr.name]["type"] = attr.type
                    unique = list(attr.metrics["freq_count"].keys())
                    top = 5
                    categorical_dict[attr.name]["Unique"] = unique[:top]

            elif attr.type == DataType.DATE.value:

                date_dict[attr.name] = dict()
                for field in self.field_render_date:
                    if field in attr.metrics.keys():
                        date_dict[attr.name][
                            self.field_render_date[field]] = attr.metrics[field]
                    else:
                        date_dict[attr.name][
                            self.field_render_date[field]] = np.nan

                    if hasattr(attr, "type"):
                        date_dict[attr.name]["type"] = attr.type

            elif attr.type == DataType.STRING.value:
                key = "freq_count"
                if key in attr.metrics.keys():
                    freq_count_metric_keys = pd.DataFrame()
                    freq_count_metric_keys[attr.name] = attr.metrics[key].keys()
                    freq_count_metric_keys["{}_count".format(attr.name)] = \
                        attr.metrics[
                            key].values()
                    freq_count_metric_keys["{}_count".format(attr.name)] = \
                        freq_count_metric_keys[
                            "{}_count".format(attr.name)].astype("int")
                    freq_count_metric_keys = \
                        freq_count_metric_keys.sort_values(
                            by="{}_count".format(attr.name),
                            ascending=False)
                    string_data = pd.concat(
                        [string_data, freq_count_metric_keys], axis=1)

            elif attr.type == DataType.TEXT.value:

                for key in attr.metrics.keys():
                    if key.endswith("gram"):
                        text_count_metric_keys = pd.DataFrame()
                        # Creating a unique name for each text attribute for
                        # storing ngram analysis metrics
                        text_col = "{}_{}".format(attr.name, key)
                        text_count_col = "{}_{}_count".format(attr.name, key)

                        text_count_metric_keys[text_col] = pd.Series(list(
                            attr.metrics[key].keys()))

                        text_count_metric_keys[text_count_col] = \
                            pd.Series(list(attr.metrics[
                                               key].values()))

                        text_count_metric_keys = \
                            text_count_metric_keys.sort_values(
                                by=text_count_col,
                                ascending=False)
                        text_data = pd.concat(
                            [text_data, text_count_metric_keys], axis=1)

        numeric_data = pd.DataFrame(numeric_dict).transpose()
        categorical_data = pd.DataFrame(categorical_dict).transpose()
        date_data = pd.DataFrame(date_dict).transpose()

        if verbose and len(numeric_data):
            print("\nNumeric Analysis:")
            display(numeric_data)
        if verbose and len(categorical_data):
            print("\nCategorical Analysis:")
            display(categorical_data)
        if verbose and len(date_data):
            print("\nDate Analysis:")
            display(date_data)
        if verbose and len(string_data):
            print("\nString Analysis:")
            display(string_data)
        if verbose and len(text_data):
            print("\nText Analysis:")
            display(text_data)

        if to_excel:
            output_path, file_name = utility.get_file_from_path_string(
                output_path,
                file_name)
            file_name = utility.set_extension(file_name, ".xlsx")
            sheets = {"numeric": numeric_data,
                      "categorical": categorical_data,
                      "date": date_data,
                      "string": string_data,
                      "text": text_data}
            utility.to_excel(output_path, file_name, sheets)

    def render_multivariate(self, verbose=True, to_excel=False,
                            output_path=None,
                            file_name=None):
        """
        Outputs explore understand data
        Args:
            verbose('bool'): If True then renders the output
            to_excel('bool'): If True saves the output to excel file
            output_path(str): path where the multivariate excel file to be
                stored
            file_name(str): file name of excel file
        """
        if not verbose and not to_excel:
            return

        if self.dataset.type != DatasetType.STRUCTURED:
            logger.info("Unsupported datatype for render_multivariate")
            return

        if verbose:
            for corr in self.field_render_multivariate:
                if corr not in self.dataset.info.metrics.keys():
                    continue
                print("{} Analysis".format(corr))
                display(
                    pd.Series(list(self.dataset.info.metrics[corr].values()),
                              index=pd.MultiIndex.from_tuples(
                                  self.dataset.info.metrics[
                                      corr].keys())).unstack())
        if to_excel:
            output_path, file_name = utility.get_file_from_path_string(
                output_path,
                file_name)
            file_name = utility.set_extension(file_name, ".xlsx")
            sheets = dict()
            for corr in self.field_render_multivariate:
                if corr not in self.dataset.info.metrics.keys():
                    continue
                corr_data = pd.Series(
                    list(self.dataset.info.metrics[corr].values()),
                    index=pd.MultiIndex.from_tuples(
                        self.dataset.info.metrics[corr].keys())).unstack()
                sheets[corr] = corr_data
            utility.to_excel(output_path, file_name, sheets)

