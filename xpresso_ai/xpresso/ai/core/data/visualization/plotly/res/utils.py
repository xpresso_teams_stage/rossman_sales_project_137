#Developed By Nalin Ahuja, nalin.ahuja@abzooba.com

import os
import string
import random

#Output Types and Paths

HTML = "html"
def_path = "./plots/"

PNG = "png"
image_path = "./images/"

PDF = "pdf"
pdf_path = "./report/"

#Function creates a folder for rendered plots

def make_container(output_path):
    if(not(os.path.isdir(str(output_path)))):
        os.makedirs(str(output_path),exist_ok=True)

#Function clears the terminal screen

def clear_terminal():
    os.system('cls' if os.name == 'nt' else 'clear')

#Function joins to lists

def concatenate_lists(list_1, list_2):
    joined_lists = []

    for elem in list_1:
        joined_lists.append(elem)
    for elem in list_2:
        joined_lists.append(elem)

    return joined_lists

#Function generates a render ID to differentiate plots

def generate_id():
    return ''.join(random.choice(string.ascii_letters + string.digits) for i in range(5))

#Function detects runtime environment

def detect_notebook():
    try:
        shell_type = get_ipython().__class__.__name__
        #Jupyter Notebook
        if shell_type == 'ZMQInteractiveShell':
            return True
    except NameError:
        #Standard Interpreter
        return False

#Function halts the program

class StopExecution(Exception):
    def _render_traceback_(self):
        pass

def halt():
    if (detect_notebook()):
        raise StopExecution
    else:
        exit()
