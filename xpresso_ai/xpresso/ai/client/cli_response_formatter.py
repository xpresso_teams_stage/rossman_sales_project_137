""" Converts client response into a print statement"""
import os
import re
import random
import pandas as pd
from click import secho
from tabulate import tabulate

from xpresso.ai.core.commons.utils.constants import OUTPUT_COLOR_GREEN, \
    OUTPUT_COLOR_RED

__all__ = ['CLIResponseFormatter']
__author__ = 'Naveen Sinha'

from xpresso.ai.core.data.visualization.utils import detect_notebook


class CLIResponseFormatter:
    """
    Takes a data and creates string in a specific format
    """
    # Key to print only value not key
    MESSAGE_KEY = "message"

    PRINT_FORMAT_HIERARCHICAL = "hierarchical"
    PRINT_FORMAT_EXPERIMENT_TABULAR = "experiment_tabular"

    # Output type
    OUTPUT_PLAIN_TEXT = "plain"
    OUTPUT_HTML = "html"

    def __init__(self):
        self.is_error = False
        self.output_type = self.OUTPUT_PLAIN_TEXT

    @staticmethod
    def format_string(name: str):

        tmp_name = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
        underscore_name = re.sub('([a-z0-9])([A-Z])', r'\1_\2',
                                 tmp_name)
        updated_name = underscore_name.replace("_", " ")
        updated_name_list = []
        for word in updated_name.split(' '):
            if word.isupper():
                updated_name_list.append(word)
            else:
                updated_name_list.append(word.title())

        return ' '.join(updated_name_list)

    def print_to_console(self, message, is_highlight=False):
        """ Prints a message in aesthetic form

        Args:
            is_highlight(bool): Whether to highlight this message in the output.
                          Uses green color for highlight
            message(str): String message which needs to be printed to console
        """
        if self.is_error:
            secho(message=message, err=self.is_error, fg=OUTPUT_COLOR_RED)
        elif is_highlight:
            secho(message=message, err=self.is_error, fg=OUTPUT_COLOR_GREEN)
        else:
            secho(message=message)

    def print_new_line(self, value, item, index):
        """
        Print new line if required. After every line and dictionary, there
        should be a new line for better aesthetics
        Args:
            value: Value which is being used
            item: It is the parent container, May be a list or dictionary
            index:  Current Index

        """
        if isinstance(value, dict) and index != len(item) - 1:
            # This is being done to add a new line after a dict/list is
            # printed
            self.print_to_console(message="")

    def print_recursively(self, item, shift=0, is_highlight=False):
        """
        Prints an complex object to the console output. To be used for pretty
        printing It creates the string in a recursive loop.

        If any item has key as "message", it highlights the message

        Args:
            is_highlight(bool): Whether to highlight this message in the output.
            item: complex object
            shift(int): number of spaces to add before printing
        """
        if not item:
            return

        if isinstance(item, dict):
            if isinstance(item, dict):
                delete_keys = [key for key in item if not item[key]]
                for key in delete_keys:
                    del item[key]

            for index, key in enumerate(list(item.keys())):
                value = item[key]
                if ((isinstance(value, list) or isinstance(value, dict))
                    and key == self.MESSAGE_KEY):
                    self.print_recursively(value, shift, is_highlight=True)
                elif isinstance(value, list) or isinstance(value, dict):
                    self.print_to_console(
                        message=f"{shift * ' '}"
                                f"{self.format_string(key)}: ",
                        is_highlight=is_highlight)
                    self.print_recursively(value, shift + 2,
                                           is_highlight=is_highlight)
                elif key == self.MESSAGE_KEY:
                    self.print_to_console(message=f"{shift * ' '}{value}",
                                          is_highlight=True)
                else:
                    self.print_to_console(
                        message=f"{shift * ' '}"
                                f"{self.format_string(key)}: {value}",
                        is_highlight=is_highlight)

                self.print_new_line(value=value, item=list(item.keys()),
                                    index=index)

        elif isinstance(item, list):
            for index, value in enumerate(item):
                self.print_recursively(value, shift, is_highlight=is_highlight)
                self.print_new_line(value=value, item=item, index=index)

        else:
            self.print_to_console(message=f"{shift * ' '}{item}",
                                  is_highlight=is_highlight)

    def update_col_rows(self, data, list_of_experiment_dict: list):
        """ Create a single root dictionary with top two level taken as the keys
        at root level """

        new_dict_item = dict()
        for key, value in data.items():
            if (isinstance(value, str)
                or isinstance(value, int)
                or isinstance(value, float)):
                new_dict_item[key] = value
            # elif not isinstance(value, dict):
            else:
                new_dict_item[key] = str(value)
            # else:
            #     for inner_key, inner_value in value.items():
            #         if(isinstance(inner_value, str)
            #             or isinstance(inner_value, int)
            #                 or isinstance(inner_value, float)):
            #             new_dict_item[f"{key}__{inner_key}"] = inner_value
            #         else:
            #             new_dict_item[f"{key}__{inner_key}"] = str(inner_value)
        list_of_experiment_dict.append(new_dict_item)

    def print_experiments_in_single_table(self, data):
        """
        Convert the list of dictionaries into table
        """
        # Get all columns
        list_of_experiment_dict = list()
        if isinstance(data, dict):
            self.update_col_rows(data=data,
                                 list_of_experiment_dict=list_of_experiment_dict)
        elif isinstance(data, list):
            for experiment_item in data:
                if not isinstance(experiment_item, dict):
                    continue
                self.update_col_rows(data=experiment_item,
                                     list_of_experiment_dict=list_of_experiment_dict)
        df = pd.DataFrame(list_of_experiment_dict)
        html_output = tabulate(df, headers='keys', floatfmt=".4f", colalign=("center",),
                               showindex=False, tablefmt="html").replace('\n', ' ')
        if detect_notebook():
            from IPython.display import HTML, display
            display(HTML(html_output))
        else:

            if self.output_type == self.OUTPUT_HTML:
                print(html_output)
            else:
                print(tabulate(df, headers='keys', floatfmt=".4f",
                               colalign=("center",), showindex=False,
                               tablefmt="simple"))



    def print_experiment_tabular(self, data):
        """ Print the data in tabular format
        It works on dict/list data. It takes top 2 level in the dictionary
        as column name and creates row from their value in all the rest of
        the dictionary
        """
        if not isinstance(data, list):
            return self.print_recursively(item=data)

        for experiment_list in data:
            self.print_experiments_in_single_table(data=experiment_list)

    def pretty_print(self, data, format_type=PRINT_FORMAT_HIERARCHICAL,
                     is_error=False, output_type=OUTPUT_PLAIN_TEXT):
        """ Generate print statement in the specified format """
        self.is_error = is_error
        self.output_type = output_type
        if format_type == self.PRINT_FORMAT_EXPERIMENT_TABULAR:
            return self.print_experiment_tabular(data)
        return self.print_recursively(data)


if __name__ == "__main__":
    test_data = {"result": {"message": "empty_list, empty_str & "
                                       "has-empty-ele.id2 shouldn't be printed",
                            "build successful": [{"id1": 1}, {"id2": 2}],
                            "Test successful": [1, 2, 3, 4],
                            "Should be s": ["1", "2", "3"],
                            "empty_list": [],
                            "empty_str": '',
                            "has-empty-ele": [{"id1": 1}, {"id2": None}]
                            },
                 "hyper": "temp",
                 "Testing": "Asd"
                 }
    CLIResponseFormatter().pretty_print(data=test_data)
    CLIResponseFormatter().pretty_print(data=test_data, is_error=True)
